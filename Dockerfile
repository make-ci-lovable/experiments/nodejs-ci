# Nodejs for GitLab CI 2020-10-29 by @k33g | on gitlab.com 
FROM node:14-alpine

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

# 🚧 wip
RUN apk --update add --no-cache curl

COPY nodejs_run.sh /usr/local/bin/nodejs_run
RUN chmod +x /usr/local/bin/nodejs_run

CMD ["/bin/sh"]


