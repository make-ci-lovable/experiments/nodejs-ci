#!/bin/sh
# https://stackoverflow.com/questions/4380478/indirect-parameter-substitution-in-shell-script
param=${1}
eval source_code=\"\$$param\"
# TODO replace
echo "$source_code" > ${1}.js
export NODE_PATH=/usr/lib/node_modules
node ${1}.js

#const fs = require("fs"); fs.writeFileSync(${1}.js, process.env.source_code, 'utf8')
#source_code='var a="hello"; console.log(`${a} world`)'
#node --eval 'const fs = require("fs"); fs.writeFileSync("yo.js", process.env.source_code, "utf8")'
